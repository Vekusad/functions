const getSum = (str1, str2) =>
{

  if (typeof (str1) != 'string' || typeof (str2) != 'string')
  {
    return false;
  }
  let num1 = Number(str1);
  let num2 = Number(str2);
  if (isNaN(num1) || isNaN(num2))
  {
    return false;
  }
  return (num1 + num2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) =>
{
  let authors = []
  let postsCount = 0;
  let commentsCount = 0;
  listOfPosts.forEach(post =>
  {
    authors.push(post.author);
  })
  listOfPosts.forEach(post =>
  {
    if (post.author == authorName)
    {
      postsCount++;
    }
  })
  listOfPosts.forEach(post =>
  {
    if (post.comments != undefined)
    {
      post.comments.forEach(comment =>
      {
        if (comment.author == authorName)
        {
          commentsCount++;
        }
      })
    }
  })


  return `Post:${postsCount},comments:${commentsCount}`;
}

const tickets = (people) =>
{
  let currentMoney = 0;
  people.forEach(element =>
  {
    if (element - 25 > currentMoney)
    {
      return "NO"
    }
  });
  return "YES"
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
